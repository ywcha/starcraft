﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starcraft
{
    
    abstract class Unit //abstract class --> 추상클래스
    {
        public int hp;
        public int AttackUpgradeGrade;
        public int ArmorUpgradeGrade;

        protected int X;
        protected int Y;
        
        protected int getDamage; //받는피해

        public virtual void Upgrade() // 공격력 , 방어력 업그레이드
        {
            
                    AttackUpgradeGrade += 1;
                    ArmorUpgradeGrade += 1;

            if (AttackUpgradeGrade >= 3)
            {
                return;
            }
        }



        public virtual void Move(int x, int y) //가상함수
        {
            if (x < 0 || y < 0)
            {
                X = x;
                Y = y;
            }
        }
        public abstract void MakeSound(); // 추상메서드


        public virtual void GetDamage(int getDamage)
        {
            if(getDamage > 0)
            {
                if(hp >= getDamage)
                {
                    hp = hp - getDamage;
                }
                else if(hp < getDamage)
                {
                    hp = 0;
                }
            }
        }
    }
}
