﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starcraft
{
    abstract class Protoss : Unit
    {
        protected int shieldHp;
        protected int shieldUpgradeGrade;
        public override void GetDamage(int getDamage)
        {
            if(getDamage > 0)
            {
                if (shieldHp >= getDamage)
                    shieldHp = shieldHp - getDamage;


                else if (shieldHp < getDamage)
                {
                        if (hp - (getDamage - shieldHp) > 0) {
                        hp = hp - (getDamage - shieldHp);
                        shieldHp = 0;
                    }
                    else
                    {
                        hp = 0;
                        shieldHp = 0;
                    }
                  
                }

                base.GetDamage(getDamage);
            }
        }

        public override void Upgrade()
        {
            AttackUpgradeGrade += 1;
            ArmorUpgradeGrade += 1;
            shieldUpgradeGrade += 1;
            if (AttackUpgradeGrade >= 3)
            {
                return;
            }
        }
    }
}

